﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace ToolQA.TestCases
{
    public class Story1
    {
        IWebDriver driver;

        [SetUp]
        public void Initialize()
        {
            driver = new ChromeDriver();
            driver.Url = "http://www.euromoneyplc.com/";
        }

        [TestCase]
        public void ManagmentTeamOpen()
        {
            driver.FindElement(By.XPath("/html/body/div[1]/header/a/span")).Click();
            Thread.Sleep(1000);
            driver.FindElement(By.XPath("//*[@id='7F0853DA9D724593BF11788E77D841BD']/a")).Click();
            Thread.Sleep(1000);
            driver.FindElement(By.XPath("//*[@id='7F0853DA9D724593BF11788E77D841BD-collapse']/li[4]/a")).Click();
            Thread.Sleep(500);
            var image = driver.FindElement(By.XPath("/html/body/section/div[2]/div/div/div/div/div/img")).GetAttribute("src");
            Assert.AreEqual(image, "https://euromoneyplc.euromoneycdn.com/v-2f2eace5914d1cfac75dc59309967cb0/Media/Images/omd/em-plc/People/management-team/ARashbass%20picture%20cropped/ARashbass2.jpg");
        }

        [TearDown]
        public void EndTest()
        {
            driver.Close();
        } 
    }
}
