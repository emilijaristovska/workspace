﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace ToolQA.TestCases
{
    public class Story2
    {
        IWebDriver driver;

        [SetUp]
        public void Initialize()
        {
            driver = new ChromeDriver();
            driver.Url = "http://www.euromoneyplc.com/";
        }

        [TestCase]
        public void IJGlobalCase()
        {
            driver.FindElement(By.XPath("/html/body/div[1]/header/a/span")).Click(); 
            Thread.Sleep(1000);
            driver.FindElement(By.XPath("//*[@id='2FF53C5412284C328FFBD91BB0E1B2C4']/a")).Click(); 
            Thread.Sleep(1000);
            driver.FindElement(By.XPath("//*[@id='2FF53C5412284C328FFBD91BB0E1B2C4-collapse']/li[5]/a")).Click(); 
            Thread.Sleep(500);
            var link = driver.FindElement(By.XPath("/html/body/section/div[15]/div/div/div/div/div[2]/p[2]/a")).GetAttribute("href"); 
            driver.Url = link;
            Assert.IsTrue(driver.FindElement(By.XPath("//*[@id=\"leagueTable\"]")).Displayed);
        }

        [TearDown]
        public void EndTest()
        {
            driver.Close();
        }
    }
}
